# agent

## Overview
Manages the configuration of the puppet.conf for agents.
Accounts for both 3.x and 4.x versions of puppet.

### parameters

#### `servername`
The puppet server name to connect to.

#### `certname`
The name of the cert to use for this client

#### `environment`
The environment to operate.  
Default: 'production'

#### `report`
Enables reporting  
Default: 'true'

#### `pluginsync`
Enables plugin sync.  
Default: 'true'

#### `runinterval`
The time between agent checkins with the master.
Note that a runinterval of 0 means “run continuously” rather than “never run.” 
This setting can be a time interval as listed below:

* seconds (30 or 30s)
* minutes (30m)
* hours (6h)
* days (2d)
* years (5y)

Default: '30m'

## agent::gpg

# Manages the gpg key for apt.
# This is based on puppetlabs-apt >= 2.0.0

### parameters
#### `key`
The gpg key for the puppet labs apt source.
Default: '6F6B15509CF8E59E6E469F327F438280EF8D349F'
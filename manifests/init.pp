# == Class: agent
#
# Manages the puppet.conf file.
#
# === Parameters
#
# [*servername*]
#   The puppet server name to connect to.
#
# [*certname*]
#   The name of the cert to use for this client
#
# [*environment*]
#   The environment to operate.
#   Default: 'production'
#
# [*report*]
#   Enables reporting
#   Default: 'true'
#
# [*pluginsync*]
#   Enables plugin sync.
#   Default: 'true'
#
# [*runinterval*]
#  The time between agent checkins with the master.
#  Note that a runinterval of 0 means “run continuously” rather than 
#  “never run.” 
#  This setting can be a time interval in seconds (30 or 30s), 
#  minutes (30m), hours (6h), days (2d), or years (5y).
#  Default: '30m'
#
# [*enable_splay*]
#   If true, sets splay to true.
#   https://docs.puppet.com/puppet/latest/reference/configuration.html#splay
#   Default: false
#
# === Examples
#
#
# === Copyright
#
#
class agent (
  $servername,
  $certname,
  $environment  = 'production',
  $report       = true,
  $pluginsync   = true,
  $runinterval  = '30m',
  $enable_splay = false,
){
  anchor{'agent::begin':}

  $versions = split($::puppetversion,'[.]')
  $version = $versions[0]

  if $version == '4' {
    $conf = '/etc/puppetlabs/puppet/puppet.conf'
  }else {
    $conf = '/etc/puppet/puppet.conf'
  }
  file {$conf:
    ensure  => file,
    content => template('agent/puppet.conf.erb'),
    require => Anchor['agent::begin'],
  }
  file {'/etc/default/puppet':
    ensure  => file,
    source  => 'puppet:///modules/agent/default',
    require => File[$conf],
  }
  anchor{'agent::end':
    require => File['/etc/default/puppet'],
  }
}

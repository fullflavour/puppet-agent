# == Class: agent::gpg
#
# Manages the gpg key for apt.
# This is based on puppetlabs-apt >= 2.0.0
#
# === Parameters
#
# [*key*]
#   The gpg key for the puppet labs apt source.
#   Default: '6F6B15509CF8E59E6E469F327F438280EF8D349F'
#
class agent::gpg (
  $key = '6F6B15509CF8E59E6E469F327F438280EF8D349F',
){
  anchor{'agent::gpg::begin':}

  apt::key { 'puppet gpg key':
    id      => $key,
    server  => 'pgp.mit.edu',
    require => Anchor['agent::gpg::begin'],
  }

  # do an update
  exec{'update':
    command     => '/usr/bin/apt-get update',
    refreshonly => true,
    subscribe   => Apt::Key['puppet gpg key'],
  }

  anchor{'agent::gpg::end':
    require => Exec['update'],
  }
}

class{'agent':
  servername => 'localhost',
  certname   => 'localhost',
}
class{'agent::gpg':
  require => Class['agent'],
}
